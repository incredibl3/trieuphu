import device;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import ui.View as View;
import src.Config as config;
import src.MainMenuScreen as MainMenuScreen;
import src.GameScreen as GameScreen;
import ui.StackView as StackView;

var BG_WIDTH = config.bgWidth;
var BG_HEIGHT = config.bgHeight;
var app;
exports = Class(GC.Application, function () {

  this.initUI = function () {

    app = this;

    this.setScreenDimensions(BG_WIDTH > BG_HEIGHT);

    var mainMenuScreen = new MainMenuScreen(),
      gameScreen = new GameScreen();


    var rootView = new StackView({
      superview: this.view,
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      scale: 1,
      clip: true,
      zIndex: 1,
      backgroundColor: "red"
    });

    //rootView.push(mainMenuScreen);
    rootView.push(gameScreen);

    var test = new TextView({
          text: "Click to dismiss!\nThis is the front view.",
          backgroundColor: "#00F", //blue,
          color: "#FFF",
          size: 20,
          autoSize: false,
          autoFontSize: false,
          wrap: true
        });

    mainMenuScreen.on('MainMenuScreen:play', function(){
      //rootView.push(test);
      rootView.push(gameScreen);
    })

  };

  this.launchUI = function () {

  };

  /**
   * setScreenDimensions
   * ~ normalizes the game's root view to fit any device screen
   */
  this.setScreenDimensions = function(horz) {
    var ds = device.screen;
    var vs = this.view.style;
    vs.width = horz ? ds.width * (BG_HEIGHT / ds.height) : BG_WIDTH;
    vs.height = horz ? BG_HEIGHT : ds.height * (BG_WIDTH / ds.width);
    vs.scale = horz ? ds.height / BG_HEIGHT : ds.width / BG_WIDTH;
  };

});
