import device;
import AudioManager;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.widget.ButtonView as ButtonView;
import src.Config.GameScreenConfig as config;
import src.lib.uiInflater as uiInflater;
import src.util as util;
import src.questions as questions;
import ui.ScoreView as ScoreView;
import effects;
import animate;

var BG_WIDTH = config.bgWidth;
var BG_HEIGHT = config.bgHeight;
var TIMEOUT = 15000;
var app;
exports = Class(ImageView, function (supr) {
  this.init = function (opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      image: "resources/images/background.png"
    });

    this._sound = new AudioManager({
      path: "resources/sounds/",
      files: {
        background_music: {
          volume: 1,
          loop: true
        },
        correct: {
          volume: 1,
          loop: false
        },
        out_of_time: {
          volume: 1,
          loop: false
        },
        touch: {
          volume: 1,
          loop: false
        },
      }
    });
    this._sound.play("background_music");
    app = this;
    supr(this, 'init', [opts]);
    console.log("hungnt game screen");

    this.score = 0;
    this.tapCount = 1;
    this.isGameOver = false;
    this.questNum = 0;
    this.spItemActivating = false;

    //Play state
    this.timerView = new View(merge({parent: this}, config.TimerView));
    this.scoreView = new ScoreView(merge({ parent: this }, config.scoreView));
    this.scoreView.setText("0/" + questions.length);
    this.gamelayer = new View(merge({parent: this, y: 0}, config.GameLayer));
    this.supportItems = new View(merge({parent: this, y: config.bgHeight - 100}, config.SupportItems));

    uiInflater.addChildren(config.TimerView.children, this.timerView);
    uiInflater.addChildren(config.GameLayer.children, this.gamelayer);
    uiInflater.addChildren(config.SupportItems.children, this.supportItems);

    //Public answer view
    this.publicView1 = new View({
      parent: this.gamelayer.answer1,
      layout: 'box',
      width: 0,
      height: 80,
      opacity: 0.7,
      backgroundColor: 'black',
      x: this.gamelayer.answer1.style.x,
      y: 0
    });
    this.publicView2 = new View({
      parent: this.gamelayer.answer2,
      layout: 'box',
      width: 0,
      height: 80,
      opacity: 0.7,
      backgroundColor: 'black',
      x: this.gamelayer.answer2.style.x,
      y: 0
    });
    this.publicView3 = new View({
      parent: this.gamelayer.answer3,
      layout: 'box',
      width: 0,
      height: 80,
      opacity: 0.7,
      backgroundColor: 'black',
      x: this.gamelayer.answer3.style.x,
      y: 0
    });
    this.publicView4 = new View({
      parent: this.gamelayer.answer4,
      layout: 'box',
      width: 0,
      height: 80,
      opacity: 0.7,
      backgroundColor: 'black',
      x: this.gamelayer.answer4.style.x,
      y: 0
    });

    //Gameover state
    this.gameoverview = new View(merge({parent: this, x: 0, y: 0, width: BG_WIDTH, height: BG_HEIGHT, zIndex: 5}));
    this.gameoverTxt = new TextView({
      superview: this.gameoverview,
      layout: 'box',
      centerX: true,
      centerY: true,
      width: BG_WIDTH,
      height: 100,
      text: "GameOver!!!",
      color: "black",
      fontFamily: "UVNBaiSau_R",
      zIndex: 6,
      size: 40
    });
    this.gameoverview.hide();

    //worm dialog
    this.spDialog = new ImageView({
      superview: this.timerView.worm,
      layout: "box",
      x: this.timerView.worm.style.x + 30,
      y: 45,
      width: 132,
      height: 60,
      zIndex: 6,
      image: "resources/images/dialog.png"
    });
    this.spDialog.hide();

    this.txtDialog = new TextView({
      superview: this.spDialog,
      layout: 'box',
      centerX: true,
      centerY: true,
      width: 132,
      height: 60,
      color: "white",
      fontFamily: "UVNBaiSau_R",
      zIndex: 6,
      size: 20
    });


    this.shuffleQuests = util.shuffle(questions);
    this.generateQuest();
    this.supportItemsFunc();
  }

  this.resetTimerView = function(){
    // this.anim.clear();
    this.timerView.progress.updateOpts({width: 80});
    this.timerView.worm.updateOpts({x: 5});
  }

  this.supportItemsFunc = function(){
    //timer
    this.supportItems.item1.on('selected', bind(this, function () {
      app._sound.play("touch");
      if(this.spItemActivating == false)
      {
        this.spItemActivating = true;
        console.log("Timer item actived");
        // this.supportItems.item1.setState(ButtonView.states.DISABLED);
        this.supportItems.item1.updateOpts({visible: false});
        // this.anim.pause();

        clearTimeout(this.spItemAnimation);
        this.animPrg.pause();
        this.animWorm.pause();
        this.spDialog.show();
        this.txtDialog.setText("+5 seconds");

        setTimeout(function(){
          app.animPrg.resume();
          app.animWorm.resume();
          // app.spDialog.updateOpts({visible: false});
          app.spDialog.hide();
          app.txtDialog.setText("");
          app.spItemActivating = false;
        }, 5000);
      }
    }));

    //public
    this.supportItems.item2.on('selected', bind(this, function () {
      app._sound.play("touch");
      // console.log("hungnt Pulic View 1: " + parseInt(this.quest[2]));
      // this.supportItems.item2.setState(ButtonView.states.DISABLED);
      if(this.spItemActivating == false)
      {
        this.spItemActivating = true;
        clearTimeout(this.spItemAnimation);
        this.supportItems.item2.updateOpts({visible: false});
        animate(this.publicView1).now({width: parseInt(this.quest[3]) / 100 * this.gamelayer.answer1.style.width, height: 80}, 1000, animate.linear);
        animate(this.publicView2).now({width: parseInt(this.quest[5]) / 100 * this.gamelayer.answer2.style.width, height: 80}, 1000, animate.linear);
        animate(this.publicView3).now({width: parseInt(this.quest[7]) / 100 * this.gamelayer.answer3.style.width, height: 80}, 1000, animate.linear);
        animate(this.publicView4).now({width: parseInt(this.quest[9]) / 100 * this.gamelayer.answer4.style.width, height: 80}, 1000, animate.linear);
        this.spDialog.show();
        this.txtDialog.setText("public's answer");
        this.spItemAnimation = setTimeout(function(){
            app.spDialog.hide();
            app.txtDialog.setText("");
            app.spItemActivating = false;
        }, 3000);
      }
    }));

    //2 taps
    this.supportItems.item3.on('selected', bind(this, function () {
      app._sound.play("touch");
      // this.supportItems.item3.setState(ButtonView.states.DISABLED);
      if(this.spItemActivating == false)
      {
        this.spItemActivating = true;
        clearTimeout(this.spItemAnimation);
        this.supportItems.item3.updateOpts({visible: false});
        this.tapCount = 2;
        this.spDialog.show();
        this.txtDialog.setText("2 taps");
        this.spItemAnimation = setTimeout(function(){
            app.spDialog.hide();
            app.txtDialog.setText("");
            app.spItemActivating = false;
        }, 3000);
      }
    }));

    //50/50
    this.supportItems.item4.on('selected', bind(this, function () {
      app._sound.play("touch");
      // this.supportItems.item4.setState(ButtonView.states.DISABLED);
      if(this.spItemActivating == false)
      {
        this.spItemActivating = true;
        clearTimeout(this.spItemAnimation);
        this.supportItems.item4.updateOpts({visible: false});
        this.spDialog.show();
        this.txtDialog.setText("50/50");
        var arrAnswer = [2, 4, 6, 8]; //Public View Answer 1-2-3-4
        var count = 0;
        var lastHiddenRow = 0;
        while(count < 2)
        {
          var rand = util.choice(arrAnswer);
          if(this.quest[rand].localeCompare(this.quest[10]) != 0 && rand != lastHiddenRow)
          {
            count++;
            lastHiddenRow = rand;
            switch (rand){
              case 2: this.gamelayer.answer1.hide(); break;
              case 4: this.gamelayer.answer2.hide(); break;
              case 6: this.gamelayer.answer3.hide(); break;
              case 8: this.gamelayer.answer4.hide(); break;
            }
          }
        };

        this.spItemAnimation = setTimeout(function(){
            app.spDialog.hide();
            app.txtDialog.setText("");
            app.spItemActivating = false;
        }, 3000);
      }
    }));
  }

  this.generateQuest = function(){
    // 0 Category
  // 1 Question
  // 2 Answer 1
  // 3 Public View Answer 1
  // 4 Answer 2
  // 5 Public View Answer 2
  // 6 Answer 3
  // 7 Public View Answer 3
  // 8 Answer 4
  // 9 Public View Answer 4
  // 10 Correct Answer

    //hide public answer view
    this.publicView1.updateOpts({width: 0});
    this.publicView2.updateOpts({width: 0});
    this.publicView3.updateOpts({width: 0});
    this.publicView4.updateOpts({width: 0});

    //show answer button
    this.gamelayer.answer1.show();
    this.gamelayer.answer2.show();
    this.gamelayer.answer3.show();
    this.gamelayer.answer4.show();

    //reset support item state
    // this.supportItems.item1.setState(ButtonView.states.UNSELECTED);
    // this.supportItems.item2.setState(ButtonView.states.UNSELECTED);
    // this.supportItems.item3.setState(ButtonView.states.UNSELECTED);
    // this.supportItems.item4.setState(ButtonView.states.UNSELECTED);

    //reset tap count
    this.tapCount = 1;

    //answer button animation
    animate(this.gamelayer.questionCategoryBG).now({width: 429, height: 109, x: 80, y: 110}, 240, animate.linear);

    animate(this.gamelayer.questionBG).now({width: config.questionBG_width, height: config.questionBG_height, x: 50, y: 140}, 200, animate.linear);

    animate(this.gamelayer.answer1).now({width: config.answerWidth, height: config.answerHeight, x: 50, y: 500}, 240, animate.linear);

    animate(this.gamelayer.answer2).then({width: config.answerWidth, height: config.answerHeight, x: 50, y: 600}, 240, animate.linear);

    animate(this.gamelayer.answer3).then({width: config.answerWidth, height: config.answerHeight, x: 50, y: 700}, 240, animate.linear);

    animate(this.gamelayer.answer4).then({width: config.answerWidth, height: config.answerHeight, x: 50, y: 800}, 240, animate.linear);


    //get random quest
    setTimeout(function(){
      app.quest = app.shuffleQuests[app.questNum++];
      app.gamelayer.categoryTxt.setText(app.quest[0]);
      app.gamelayer.questionTxt.setText(app.quest[1] /* + "(answer:" + app.quest[10]+ ")"*/);
      app.gamelayer.answer1.setTitle(app.quest[2]);
      app.gamelayer.answer2.setTitle(app.quest[4]);
      app.gamelayer.answer3.setTitle(app.quest[6]);
      app.gamelayer.answer4.setTitle(app.quest[8]);
    }, 250);


    //handle answer button events
    this.gamelayer.answer1.onInputSelect = bind(this, function(){
      if(this.gamelayer.answer1.getText().getText().localeCompare(this.quest[10]) == 0){
        app._sound.play("correct");
        this.updateScoreAndQuest();
      }else{
        if(this.tapCount == 1)
        {
          app._sound.play("out_of_time");
          this.endGame("GameOver!!! Your score is " + this.score);
        }
        else
        {
          this.tapCount--;
          this.gamelayer.answer1.hide();
        }
      }
    });
    this.gamelayer.answer2.onInputSelect = bind(this, function(){
      if(this.gamelayer.answer2.getText().getText().localeCompare(this.quest[10]) == 0){
        app._sound.play("correct");
        this.updateScoreAndQuest();
      }else{
        if(this.tapCount == 1)
        {
          app._sound.play("out_of_time");
          this.endGame("GameOver!!! Your score is " + this.score);
        }
        else
        {
          this.tapCount--;
          this.gamelayer.answer2.hide();
        }
      }
    });
    this.gamelayer.answer3.onInputSelect = bind(this, function(){
      if(this.gamelayer.answer3.getText().getText().localeCompare(this.quest[10]) == 0){
        app._sound.play("correct");
        this.updateScoreAndQuest();
      }else{
        if(this.tapCount == 1)
        {
          app._sound.play("out_of_time");
          this.endGame("GameOver!!! Your score is " + this.score);
        }
        else
        {
          this.tapCount--;
          this.gamelayer.answer3.hide();
        }
      }
    });

    this.title = this.gamelayer.answer4.getText().getText();
    this.gamelayer.answer4.onInputSelect = bind(this, function(){
      if(this.gamelayer.answer4.getText().getText().localeCompare(this.quest[10]) == 0){
        app._sound.play("correct");
        this.updateScoreAndQuest();
      }else{
        if(this.tapCount == 1)
        {
          app._sound.play("out_of_time");
          this.endGame("GameOver!!! Your score is " + this.score);
        }
        else
        {
          this.tapCount--;
          this.gamelayer.answer4.hide();
        }
      }
    });

    //start timer view
    // this.anim = animate(this.timerView).now({width: 0}, TIMEOUT, animate.linear).then(this.gameOver);
    this.animPrg = animate(this.timerView.progress).now({width: BG_WIDTH-20}, TIMEOUT, animate.linear).then(bind(this, function(){
      app._sound.play("out_of_time");
      this.endGame("GameOver!!! Your score is " + this.score);
    }));
    this.animWorm = animate(this.timerView.worm).now({x: BG_WIDTH-100}, TIMEOUT, animate.linear);
  }


  this.updateScoreAndQuest = function() {
    this.scoreView.setText(++this.score + "/" + questions.length);
    //effects.spin(this.gamelayer, {duration: 1000, loop: false});

    //hide public answer view
    this.publicView1.updateOpts({width: 0});
    this.publicView2.updateOpts({width: 0});
    this.publicView3.updateOpts({width: 0});
    this.publicView4.updateOpts({width: 0});

    //hide current question, answer button sequently
    this.gamelayer.categoryTxt.setText("");
    this.gamelayer.questionTxt.setText("");
    this.gamelayer.answer1.setTitle("");
    this.gamelayer.answer2.setTitle("");
    this.gamelayer.answer3.setTitle("");
    this.gamelayer.answer4.setTitle("");

    animate(this.gamelayer.questionCategoryBG).now({width: 0, height: 0,
    x: this.gamelayer.questionCategoryBG.style.x + this.gamelayer.questionCategoryBG.style._width/2,
    y: this.gamelayer.questionCategoryBG.style.y + this.gamelayer.questionCategoryBG.style._height/2}, 200, animate.linear);

    animate(this.gamelayer.questionBG).now({width: 0, height: 0,
    x: this.gamelayer.questionBG.style.x + this.gamelayer.questionBG.style._width/2,
    y: this.gamelayer.questionBG.style.y + this.gamelayer.questionBG.style._height/2}, 200, animate.linear);

    animate(this.gamelayer.answer1).now({width: 0, height: 0,
    x: this.gamelayer.answer1.style.x + this.gamelayer.answer1.style._width/2,
    y: this.gamelayer.answer1.style.y + this.gamelayer.answer1.style._height/2}, 200, animate.linear);

    animate(this.gamelayer.answer2).wait(100).then({width: 0, height: 0,
    x: this.gamelayer.answer2.style.x + this.gamelayer.answer2.style._width/2,
    y: this.gamelayer.answer2.style.y + this.gamelayer.answer2.style._height/2}, 200, animate.linear);

    animate(this.gamelayer.answer3).wait(100).then({width: 0, height: 0,
    x: this.gamelayer.answer3.style.x + this.gamelayer.answer3.style._width/2,
    y: this.gamelayer.answer3.style.y + this.gamelayer.answer3.style._height/2}, 200, animate.linear);

    animate(this.gamelayer.answer4).wait(100).then({width: 0, height: 0,
    x: this.gamelayer.answer4.style.x + this.gamelayer.answer4.style._width/2,
    y: this.gamelayer.answer4.style.y + this.gamelayer.answer4.style._height/2}, 200, animate.linear);

    // generate new quest
    setTimeout(function(){
      app.resetTimerView();
      if(app.questNum == questions.length)
        app.endGame("Congrats!!! You win, your score is: " + app.score);
      else
        app.generateQuest();
    }, 1000);

  }

  this.endGame = function(txt){
    app.isGameOver = true;
    app.gameoverview.show();
    // app.gameoverTxt.setText("GameOver!!! Your score is " + app.score);
    this.animPrg.clear();
    this.animWorm.clear();
    app.gameoverTxt.setText(txt);

    app.gameoverview.on('InputSelect', function (event, point) {
      if(app.isGameOver == true)
      {
        app.isGameOver = false;
        app.spItemActivating = false;
        app.gameoverview.hide();
        app.score = 0;
        app.scoreView.setText(app.score);
        app.questNum = 0;
        app.scoreView.setText("0/" + questions.length);
        effects.stop();
        app.resetTimerView();
        app.shuffleQuests = util.shuffle(questions);
        app.generateQuest();
        app.supportItems.item1.updateOpts({visible: true});
        app.supportItems.item2.updateOpts({visible: true});
        app.supportItems.item3.updateOpts({visible: true});
        app.supportItems.item4.updateOpts({visible: true});
      }
    });
    effects.radial(app.gameoverview);
  }
});
