exports = [
	// 0 Category
	// 1 Question
	// 2 Answer 1
	// 3 Public View Answer 1
	// 4 Answer 2
	// 5 Public View Answer 2
	// 6 Answer 3
	// 7 Public View Answer 3
	// 8 Answer 4
	// 9 Public View Answer 4
	// 10 Correct Answer

	//Question 1
	[
		"Văn học",
		"Trong truyện ngắn ông lão đánh cá và con cá vàng, ông lão đã ra biển mấy lần ? ",
		"3",
		"10",
		"4",
		"20",
		"6",
		"40",
		"5",
		"30",
		"6"
	],
	//Question 2
	[
		"Địa lý",
		"Trục của trái đất hợp với mặt phẳng xích đạo một góc là bao nhiêu?",
		"68.5",
		"10",
		"67.5",
		"10",
		"66.5",
		"60",
		"54.5",
		"20",
		"66.5"
	],
	//Question 3
	[
		"Khoa học",
		"Ai là người đầu tiên chứng minh trái đất có dạng hình cầu ?",
		"Copecnich",
		"25",
		"Aristotle",
		"50",
		"Acsimet",
		"10",
		"Galileo",
		"15",
		"Aristotle"
	],
	//Question 4
	[
		"Thể thao",
		"Cho đến năm 2007 Thái Lan đã tổ chức bao nhiêu lần đại hội SEA Games ?",
		"3",
		"0",
		"4",
		"20",
		"5",
		"10",
		"6",
		"70",
		"6"
	],
	//Question 5
	[
		"Khoa học",
		"Ở mắt bão không khí chuyển động như thế nào ?",
		"Theo chiều kim đồng hồ",
		"5",
		"không chuyển động",
		"55",
		"Ngược chiều kim đồng hồ",
		"20",
		"Tuỳ vào cơn bão",
		"20",
		"không chuyển động"
	],
	//Question 6
	[
		"Lịch sử",
		"Ai đã khắc chữ ” Nam thiên đệ Nhất động “ lên của động hương tích ?",
		"Chúa Trịnh Sâm",
		"90",
		"Chu Mạnh Trinh",
		"5",
		"Vua Bảo Đại",
		"5",
		"Nam Phương Hoàng hậu",
		"0",
		"Chúa Trịnh Sâm"
	],
	//Question 7
	[
		"Địa lý",
		"Hồ nước ngọt nào lớn nhất theo thê tích và sâu nhất thê giới ?",
		"Hồ Victoria",
		"5",
		"Hồ Ladoga",
		"15",
		"Hồ Lhagba Pool",
		"15",
		"Hồ Baikal",
		"65",
		"Hồ Baikal"
	],
	//Question 8
	[
		"Địa lý",
		"Nơi có hải cảng sầm uất nhất thế giới, bạn cho biết tên thành phố này ?",
		"London",
		"5",
		"Thượng Hải",
		"75",
		"New York",
		"10",
		"Trạn Châu Cảng",
		"10",
		"Thượng Hải"
	],
	//Question 9
	[
		"Khoa học",
		"Con sên có mấy cái mũi?",
		"4",
		"100",
		"3",
		"0",
		"2",
		"0",
		"1",
		"0",
		"4"
	],
	//Question 10
	[
		"Văn học",
		"Ông tổ của ” Đờn ca tài tử ” ?",
		"Nguyễn văn Chánh",
		"5",
		"Cao Bá Long",
		"5",
		"Nguyễn Thượng Hiền",
		"5",
		"Cao Văn Lầu",
		"85",
		"Cao Văn Lầu"
	],
	//Question 11
	[
		"Thể thao",
		"CLb Real Madrid đã bao nhiêu lần vô địch Cúp C1 Châu Âu?",
		"3",
		"20",
		"9",
		"40",
		"8",
		"20",
		"5",
		"20",
		"9"
	],
	//Question 12
	[
		"Sinh học",
		"Cây Ca cao có nguồn gốc từ châu lục nào ?",
		"Châu Á",
		"10",
		"Châu Âu",
		"20",
		"Châu Phi",
		"20",
		"Châu Mỹ",
		"50",
		"Châu Mỹ"
	],
	//Question 13
	[
		"Sinh học",
		"Đặc điểm nào sau đây ở người gắn liền với sự  phát triền của tiếng nói ?",
		"Răng Nanh kém phát triển",
		"10",
		"Trán rông và thẳng",
		"10",
		"Xương hàm  dưới  cằm lồi rõ",
		"70",
		"Cờ hốc Mắt không có ",
		"10",
		"Xương hàm  dưới  cằm lồi rõ"
	],
	//Question 14
	[
		"Khái quát",
		"Năm 1928, nhân vật hoạt hình nào lần đầu tiên xuất hiễn ?",
		"Chuột Mickey",
		"60",
		"Vit Donald",
		"30",
		"Thuỷ  Thủ Popeye",
		"10",
		"Tom & jerry ",
		"0",
		"Chuột Mickey"
	],
	//Question 15
	[
		"Văn học",
		"Thuật Ngữ Tôn giáo co nguồn gốc từ đâu ?",
		"Việt nam",
		"20",
		"phương tây",
		"70",
		"Trung Quốc",
		"10",
		"tất Cả Dều Đúng",
		"0",
		"phương tây"
	],
	//Question 16
	[
		"Địa lý",
		"Núi lửa hoạt động lớn nhất thế giới hiện nay nằm ở đâu ?",
		"Quần Đảo hawail",
		"60",
		"Nhật Bản",
		"10",
		"Philippines",
		"20",
		"Indonesia",
		"10",
		"Quần Đảo hawail"
	],
	//Question 17
	[
		"Lịch sử",
		"Văn Miếu, trường đại học đầu tiên của Việt Nam, được xây dựng dưới triều đại nào ?",
		"Nhà Họ Trần",
		"15",
		"Nhà Họ Lý",
		"55",
		"Nhà Họ Ngô",
		"25",
		"Nhà Họ Nguyễn",
		"5",
		"Nhà Họ Lý"
	],
	//Question 18
	[
		"Giải trí",
		"Hoa Hậu Hoàng Vũ năm 2007 thuộc Quốc gia nào ?",
		"Trung Quốc",
		"5",
		"Velezela",
		"5",
		"Nhật Bản",
		"85",
		"Mỹ",
		"5",
		"Nhật Bản"
	],
	//Question 19
	[
		"Thể thao",
		"Câu Lạc Bộ nào được liên đoàn bóng đá thế giớ FIFA chọn là câu lạc bộ xuất sắc nhất thế kỷ 20 ?",
		"Manchester United",
		"15",
		"Barcelona",
		"25",
		"Real Madrid",
		"35",
		"Juventus",
		"25",
		"Real Madrid"
	],
	//Question 20
	[
		"Địa lý",
		"Đất nước nào trên thế giới có hình dạng giống quả ớt ?",
		"Cuba",
		"20",
		"Chile",
		"60",
		"Brazil",
		"10",
		"Lào",
		"10",
		"Chile"
	],
	//Question 21
	[
		"Văn học",
		"Tác giả Của bài thơ ” Nhớ Con Sông Quê hương” ?",
		"Nguyễn Thi",
		"20",
		"Đỗ Trung Quân",
		"20",
		"Tế Hanh",
		"55",
		"Quang Dũng",
		"5",
		"Tế hanh"
	],
	//Question 22
	[
		"Thể thao",
		"Câu Lac bộ Bóng Đá nào được bình chọn là câu lạc bộ vĩ đại nhất thế kỷ 21 ?",
		"Tây Ban Nha",
		"0",
		"Manchester United",
		"20",
		"Argentina",
		"15",
		"Real Madrid",
		"65",
		"Real Madrid"
	],
	//Question 23
	[
		"Kinh Tế",
		"Tên tiếng anh viết tắt hệ thống tiền tệ Châu âu là gì ?",
		"EMU",
		"10",
		"EMS",
		"70",
		"EFTA",
		"10",
		"ECU",
		"10",
		"EMS"
	],
	//Question 24
	[
		"Lịch sử",
		"Hoàng hậu Mari Antoanet  Của Nước pháp  là vợ của vua nào ?",
		"Luis 16",
		"80",
		"Luis 15",
		"10",
		"Luis 14",
		"10",
		"Vua khác",
		"0",
		"Luis 16"
	],
	//Question 25
	[
		"Lịch sử",
		"Vị Trạng Nguyên nhỏ tuổi nhất trong lịch sử Việt Nam ?",
		"Nguyễn Hiền",
		"80",
		"Lê Văn Thịnh",
		"10",
		"Vũ Tuấn Chiêu",
		"10",
		"Nguyễn Kì",
		"0",
		"Nguyễn Hiền"
	],
	//Question 26
	[
		"Khoa học",
		"Hiên tưởng khi mặt trời, trái đất và mặt trăng theo thứ tự cùng nằm trên đường thẳng",
		"Hoàng Hôn",
		"0",
		"Nguyệt Thực",
		"55",
		"Giao Thừa",
		"0",
		"Nhật Thực",
		"45",
		"Nguyệt Thực"
	],
	//Question 27
	[
		"Khoa học",
		"Chuyến bay đầu tiên thế giới của 2 anh em nha Smith đã nối liền 2 châu lục nào ?",
		"ChâuPhi – ChâuMỹ",
		"30",
		"ChâuÂu- Châu Á",
		"10",
		"ChâuĐạiDương- ChâuÂu",
		"50",
		"ChâuMỹ-ChâuĐạiDương",
		"10",
		"ChâuĐạiDương- ChâuÂu"
	],
	//Question 28
	[
		"Địa lý",
		"Sông Mekong bắt nguồn từ đâu ?",
		"Trung Quốc",
		"65",
		"Lào",
		"15",
		"Campuchia",
		"10",
		"Việt Nam",
		"10",
		"Trung Quốc"
	],
	//Question 29
	[
		"Vật lý",
		"Một mét khối nước tương đương với bao nhiêu lít nước ?",
		"10",
		"5",
		"100",
		"10",
		"10000",
		"35",
		"1000",
		"50",
		"1000"
	],
	//Question 30
	[
		"Thể thao",
		"Đội Bóng xếp thứ 3 giải Ngoại hạng anh mùa bóng 2006-2007 ?",
		"Liverpool",
		"55",
		"Manchester United",
		"30",
		"Asenal",
		"10",
		"Chelsea",
		"5",
		"Liverpool"
	],
	//Question 31
	[
		"Lịch sử",
		"Bộ tem bưu chính đầu tiên của Việt Nam in hình gì ?",
		"Hình Bông Sen",
		"20",
		"Hình Chùa một Cột",
		"10",
		"Chân dung Hồ Chí Minh",
		"60",
		"Hình Hồ Gươm",
		"10",
		"Chân dung Hồ Chí Minh"
	],
	//Question 32
	[
		"Khoa học",
		"Điều khiển từ xa được phát minh vào năm nào ?",
		"1828",
		"30",
		"1898",
		"50",
		"1788",
		"10",
		"1981",
		"10",
		"1898"
	],
	//Question 33
	[
		"Địa lý",
		"Đỉnh núi cao nhất nước ta Phan Xi Păng thuộc tỉnh nào ?",
		"Sơn la",
		"30",
		"Lào Cai",
		"50",
		"Cao Bằng",
		"10",
		"Bắc Cạn",
		"10",
		"Lào Cai"
	],
	//Question 34
	[
		"Lịch sử",
		"Có bao nhiêu vị trạng nguyên dưới thời vua Gia Long ?",
		"1",
		"30",
		"2",
		"20",
		"Không có trạng nguyên",
		"40",
		"10",
		"10",
		"Không có trạng nguyên"
	],
	//Question 35
	[
		"Văn học",
		"Bài thơ ”Dương phụ hành” do ai sáng tác ?",
		"Chế lan viên",
		"30",
		"Trần Xuân Ẩn",
		"20",
		"Cao Bá Quát",
		"40",
		"Nguyễn Văn Tố",
		"10",
		"Cao Bá Quát"
	],
	//Question 36
	[
		"Khoa học",
		"Quốc Gia có lượng khí thải lớn nhất thế giới ?",
		"Đức",
		"30",
		"Mỹ",
		"20",
		"Australia",
		"40",
		"Anh",
		"10",
		"Australia"
	],
	//Question 37
	[
		"Địa lý",
		"Người lao động đất nước nào không phải cần đóng thuế ?",
		"Thụy Sỹ",
		"20",
		"Kuwait",
		"10",
		"Singapore",
		"10",
		"Vatican",
		"60",
		"Vatican"
	],
	//Question 38
	[
		"Vật lý",
		"Nguyên tử cacbon có bao nhiêu electron ?",
		"4",
		"20",
		"6",
		"60",
		"8",
		"10",
		"9",
		"10",
		"6"
	],
	//Question 39
	[
		"Lịch sử",
		"Ngày truyền thống HSSV Việt Nam là ngày nào ?",
		"30-4",
		"10",
		"21-10",
		"10",
		"9-1",
		"70",
		"22-3",
		"10",
		"9-1"
	],
	//Question 40
	[
		"Khoa Học",
		"Người ta cho thêm chì vào trong xăng nhằm mục đích gì ?",
		"Tăng Thề Tích Của xăng",
		"10",
		"Chống Cháy Nổ",
		"70",
		"Giảm Ô nhiễm Môi Trường",
		"10",
		"Giảm Bay Hơi",
		"10",
		"Chống Cháy Nổ"
	],
	//Question 41
	[
		"Địa lý",
		"Ngày Hạ Chí là ngày nào ?",
		"23-9",
		"10",
		"22-4",
		"10",
		"22-6",
		"70",
		"23-5",
		"10",
		"22-6"
	],
	//Question 42
	[
		"Sinh học",
		"Ở loài Chó tuyến mồ hôi tập trung hầu hết ở đâu ?",
		"Bụng",
		"20",
		"Tao",
		"10",
		"Đầu",
		"10",
		"Lưỡi",
		"60",
		"Lưỡi"
	],
	//Question 43
	[
		"Thể thao",
		"Trong thế kỷ XX nước Tây Ban Nha tổ chức Worlcup vào năm nào ?",
		"1991",
		"20",
		"1988",
		"10",
		"1982",
		"60",
		"1983",
		"10",
		"1982"
	],
	//Question 44
	[
		"Văn học",
		"Hai câu thơ ” Chốn xưa xe ngựa hồn thu thảo.đền cũ lâu đài bóng tịch dương là của  tác giả nào ?",
		"Hồ Xuân Hương",
		"20",
		"Đoàn Thị Điểm",
		"10",
		"Bà Huyện Thanh Quan",
		"60",
		"Thâm Tâm",
		"10",
		"Bà Huyện Thanh Quan"
	],
	//Question 45
	[
		"Thể thao",
		"Đội tuyển Bóng đá nước nào vô địch Euro năm 1976 ?",
		"Đức",
		"20",
		"Italy",
		"10",
		"Không phải 3 Đội Trên",
		"60",
		"Pháp",
		"10",
		"Không phải 3 Đội Trên"
	],
	//Question 46
	[
		"Địa lý",
		"Việt Nam có bao nhiêu bảo tồn sinh thái ?",
		"5",
		"20",
		"2",
		"10",
		"60",
		"10",
		"61",
		"60",
		"61"
	],
	//Question 47
	[
		"Lịch sử",
		"Bạn cho biết Vua Lê nào có tên tuổi rực rỡ nhất trong thời kỳ trị nước ?",
		"Lê Hiền Tông",
		"10",
		"Lê Thánh Tông",
		"60",
		"Lê Thái Tổ",
		"10",
		"Lê nhân Tông",
		"20",
		"Lê Thánh Tông"
	],
	//Question 48
	[
		"Văn học",
		"Ao thu lạnh lẽo nước trong veo ?",
		"Thu Điếu",
		"70",
		"Thu Vịnh",
		"10",
		"Thu Ẩm",
		"10",
		"Không Phải",
		"10",
		"Thu Điếu"
	],
	//Question 49
	[
		"Địa lý",
		"Đại dương có diện tích nhỏ nhất trên thế giới ?",
		"Bắc Băng Dương",
		"70",
		"Đại tây Dương",
		"10",
		"Ấn Độ Dương",
		"10",
		"Thái Bình Dương",
		"10",
		"Bắc Băng Dương"
	],
	//Question 50
	[
		"Sinh học",
		"Tên gọi khác Của quả mướp đắng ?",
		"Mướp hương",
		"30",
		"mướp Lai",
		"10",
		"Bầu",
		"10",
		"Khổ Qua",
		"5",
		"Khổ Qua"
	],
	//Question 51
	[
		"Kinh tế",
		"Bạn Cho Biết từ năm 2001, mức lương tồng thống mỹ là bao nhiêu USD ?",
		"300 Ngàn USD",
		"30",
		"500 Ngàn USD",
		"30",
		"400 Ngàn USD",
		"40",
		"600 Ngàn USD",
		"0",
		"400 Ngàn USD"
	],
	//Question 52
	[
		"Địa lý",
		"Loại Đất Có Diện Tích Lớn nhất Đồng bằng sông Cửu Long ?",
		"Đất Phù Sa",
		"10",
		"Đất Đầm Lầy",
		"10",
		"Đất Mặn",
		"10",
		"Đất Phèn",
		"70",
		"Đất Phèn"
	],
	//Question 53
	[
		"Lịch sử",
		"Tháp eiffel được hoàn tất vào năm nào ?",
		"1905",
		"20",
		"1883",
		"10",
		"1889",
		"60",
		"1833",
		"10",
		"1889"
	],
	//Question 54
	[
		"Ca nhạc",
		"Nhạc Sỹ Trịnh Công Sơn Đoạt giải ” Đĩa vàng” Tại Nhật Với Ca khúc Nào ?",
		"Bông hồng Cài Áo",
		"20",
		"Ngủ Đi Con",
		"60",
		"huyền thoại mẹ",
		"10",
		"Nguồn Cội",
		"10",
		"Ngủ Đi Con"
	],
	//Question 55
	[
		"Địa lý",
		"Biển nội  Địa có diện tích rộng nhất ?",
		"Biển Caspi",
		"20",
		"Địa Trung Hải",
		"60",
		"Biển Hồng Hải",
		"10",
		"Thái Bình Dương",
		"10",
		"Địa Trung Hải"
	],
	//Question 56
	[
		"Địa lý",
		"Ngày Thu Phân Là Ngày Nao ?",
		"23-9",
		"70",
		"25-10",
		"10",
		"12-4",
		"10",
		"23-5",
		"10",
		"23-9"
	],
	//Question 57
	[
		"Lịch sử",
		"Quốc hiệu Đại Việt Chính Thức Bắt đầu từ năm nào ?",
		"1054",
		"70",
		"1053",
		"10",
		"1052",
		"10",
		"1055",
		"10",
		"1054"
	],
	//Question 58
	[
		"Sinh học",
		"trong quang hợp ánh sáng màu gì có kha năng cung cấp nhiều năng lượng nhất cho cây ?",
		"Tất Cả Đều Cho kết Quả Như nhau",
		"30",
		"Tia Hông Ngoại và Tia Cực Tím",
		"10",
		"Vàng,Lục,DaCam",
		"10",
		"Xanh,Đỏ ,Tím",
		"50",
		"Xanh,Đỏ ,Tím"
	],
	//Question 59
	[
		"Thể thao",
		"Đội Bóng bóng nào Đã Vô Địch WC 1998 ?",
		"Đức",
		"30",
		"Brazil",
		"10",
		"Italia",
		"10",
		"Pháp",
		"50",
		"Pháp"
	],
	//Question 60
	[
		"Lịch sử",
		"Bác hồ  Phát Động tết trồng cây  vào năm nào ?",
		"1965",
		"30",
		"1961",
		"10",
		"1960",
		"50",
		"1954",
		"10",
		"1960"
	],
	//Question 61
	[
		"Nghệ thuật",
		"Phim Đầu Tiên Của Người Việt ?",
		"Trọn Tình",
		"10",
		"Đồng Tiền Kẽm Tậu Được",
		"50",
		"Cánh Đồng Ma",
		"20",
		"Cánh Đồng Hoang",
		"20",
		"Đồng Tiền Kẽm Tậu Được"
	],
	//Question 62
	[
		"Thể thao",
		"Điểm Thi Đấu Sea Games Phải Treo bao Nhiêu Lá Cờ ?",
		"8",
		"10",
		"10",
		"20",
		"14",
		"50",
		"18",
		"20",
		"14"
	],
	//Question 63
	[
		"Sinh học",
		"Cột Sống Hình Chữ S Ở Người Liên hoang đến hoạt Động Nào Sau Đây ?",
		"Phát Triển Tiếng Nói",
		"10",
		"Đi Đứng thẳng",
		"50",
		"Sử Dụng Công Cụ Lao Động",
		"20",
		"Tư Duy Trừu Tượng ",
		"20",
		"Đi Đứng thẳng"
	],
	//Question 64
	[
		"Thể thao",
		"Bóng Bàn là Một trong những môn thể thao phổ biến nhất trên thế giới, nó có nguồn gốc nước nào?",
		"Pháp",
		"10",
		"Trung Quốc",
		"10",
		"Hy Lạp",
		"10",
		"Anh",
		"70",
		"Anh"
	],
	//Question 65
	[
		"Địa lý",
		"Lá Cờ Nước mỹ Có Bao Nhiêu Ngôi Sao ?",
		"45",
		"10",
		"51",
		"30",
		"50",
		"50",
		"100",
		"10",
		"50"
	],
	//Question 66
	[
		"Địa lý",
		"Thủ Đô Canada Là ?",
		"Quebec",
		"10",
		"Ottawa",
		"60",
		"Ontario",
		"20",
		"Montreal",
		"10",
		"Ottawa"
	],
	//Question 67
	[
		"Nghệ thuật",
		"Bộ phim đầu tiên mà lời thoại trong phim có thể nghe được là  bộ phim nào ?phim này sản xuất năm 1927 ?",
		"Steam Boat",
		"10",
		"Ca Sĩ Nhac jazz",
		"80",
		"Bạch Tuyết và 7 Chú Lùn",
		"0",
		"Mickey  Mouse",
		"10",
		"Ca Sĩ Nhac jazz"
	],
	//Question 68
	[
		"Nghệ thuật",
		"Bài hát ” Lên Ngàn” là của tác giả nào ?",
		"Trần Hiếu",
		"10",
		"Trần Tiến",
		"10",
		"Hoàng Việt",
		"70",
		"Không biết Tác giả",
		"10",
		"Hoàng Việt"
	],
	//Question 69
	[
		"Địa lý",
		"Huyện Nga Son , Tỉnh thanh Hoá Nổi Tiếng Với Nghề Thủ Công Nào ?",
		"Dệt Chiếu",
		"40",
		"Dệt Vải",
		"10",
		"Làm Chiếu",
		"30",
		"Làm Nón",
		"20",
		"Dệt Chiếu"
	],
	//Question 70
	[
		"Lịch sử",
		"Bạn cho biết Tháp Rùa được xây dựng thế kỷ nào ?",
		"15",
		"10",
		"16",
		"10",
		"17",
		"10",
		"18",
		"70",
		"18"
	],
	//Question 71
	[
		"Lịch sử",
		"Làng yên phụ ở Hà Nội  Nổi tiếng với Nghề gì ?",
		"Kim Hoàn",
		"10",
		"Làm Cốm",
		"10",
		"Làm Hương",
		"60",
		"Đúc  Đồng",
		"20",
		"Làm Hương"
	],
	//Question 72
	[
		"Sinh học",
		"Chân gà Điểu Có bao nhiêu móng ?",
		"1",
		"10",
		"2",
		"60",
		"3",
		"20",
		"4",
		"10",
		"2"
	],
	//Question 73
	[
		"Văn học",
		"Bài phú Nổi Tiếng trong lịch sử văn hôc việt nam ” Ngọc Tỉnh Liên ” Là Của Vua trạng Nguyên nào ?",
		"Nguyễn Hiền",
		"10",
		"Mac Đỉnh Chi",
		"60",
		"Trương Hán Siêu",
		"20",
		"Lê văn Thịnh",
		"10",
		"Mac Đỉnh Chi"
	],
	//Question 74
	[
		"Giải trí",
		"Danh Hiệu Hoa Hậu Thế GiớiNăm 2006 thuộc về Nước nào ?",
		"Venezuela",
		"10",
		"CH Czech",
		"60",
		"Thồ Nhĩ Kỳ",
		"20",
		"Ba Lan",
		"10",
		"CH Czech"
	],
	//Question 75
	[
		"Vật lý",
		"Thiên Thể Nào lớn đễn Nổi Ngay Cả ánh sángcũng không  thoát khỏi trọng lực của nó ?",
		"Hòng Ánh Sáng",
		"10",
		"Hố Sâu",
		"10",
		"Hố Đen",
		"70",
		"Loại Khác",
		"10",
		"Hố Đen"
	],
	//Question 76
	[
		"Địa lý",
		"Quốc Ngữ Của Singapore ?",
		"Trung",
		"10",
		"Tamil",
		"10",
		"Singlish",
		"10",
		"Malaysia",
		"70",
		"Malaysia"
	],
	//Question 77
	[
		"Văn học",
		"Bài Hát Trong Phim ” Vợ Chồng A Phủ ” Là Gì ?",
		"Bài Ca Trên Núi",
		"70",
		"Huyền Thoại Hồ Núi Cốc",
		"10",
		"Chảy Đi Sông ơi",
		"10",
		"Dư Âm",
		"10",
		"Bài Ca Trên Núi"
	],
	//Question 78
	[
		"Lịch sử",
		"Vào những năm 1850 – 1870 , Nước nào có nền công nghiệp đứng đầu thế giới và được coi là” công xưởng của thế giới ?",
		"Đức",
		"30",
		"Pháp",
		"10",
		"Mỹ",
		"10",
		"Anh",
		"50",
		"Anh"
	],
	//Question 79
	[
		"Thể thao",
		"Cầu Thủ Việt Nam nào sau đây có tên trong danh sách đề cử Quả bóng vàng châu á 2008 ?",
		"Tài Em",
		"30",
		"Như Thành",
		"60",
		"Minh Phương",
		"10",
		"Minh Châu",
		"0",
		"Như Thành"
	],
	//Question 80
	[
		"Văn học",
		"Cụm Từ Nào Còn Thiếu trong câu thơ sau của Huy Cận : ” Thuyền về nước lại sầu trăm ngả,…… Lạc máy dòng ” ?",
		"Một Cánh Bèo Trôi",
		"30",
		"Một Cánh Bèo Đơn",
		"10",
		"Ghỗ Lạc Rừng Xa",
		"10",
		"Củi Một Cành Khô",
		"50",
		"Củi Một Cành Khô"
	],
	//Question 81
	[
		"Nghệ thuật",
		"Giải thưởng ” Cánh Diều vàng” Đã được tổ chức lần đầu tiên Vào năm nào ?",
		"1996",
		"10",
		"1998",
		"10",
		"2002",
		"70",
		"1994",
		"10",
		"2002"
	],
	//Question 82
	[
		"Địa lý",
		"Cảng Lớn Nhất Đông Nam á Là Cảng Nào ?",
		"Việt Nam",
		"10",
		"Lào",
		"10",
		"Singapore",
		"70",
		"Thái Land",
		"10",
		"Singapore"
	],
	//Question 83
	[
		"Vật lý",
		"1 phần tử mêtan Có bao nhiêu nguyên hidro ?",
		"4",
		"55",
		"3",
		"15",
		"2",
		"20",
		"6",
		"10",
		"4"
	],
	//Question 84
	[
		"Lịch sử",
		"Dưới thời nhà trần , ai là thầy giáo , nhà nho được nhiều người  trọng dụng nhất ?",
		"Chu Văn An",
		"55",
		"Trương Hán Siêu",
		"15",
		"Nguyễn Trãi",
		"20",
		"Phạm Sư Mạnh",
		"10",
		"Chu Văn An"
	],
	//Question 85
	[
		"Y học",
		"Ca Ghép Thận Thành Công Đầu Tiên ở việt nam thực hiện vào năm nào ?",
		"1990",
		"10",
		"1991",
		"5",
		"1992",
		"70",
		"1993",
		"15",
		"1992"
	],
	//Question 86
	[
		"Y học",
		"Tim người bình thường đập bao nhiêu nhịp một ngày ?",
		"50000",
		"20",
		"200000",
		"5",
		"150000",
		"10",
		"100000",
		"65",
		"100000"
	],
	//Question 87
	[
		"Nghệ thuật",
		"Thụy Điển  nổi tiếng với thể loại nhac nào ?",
		"Dance",
		"20",
		"Jazz",
		"5",
		"Pop",
		"60",
		"Cổ Điển",
		"15",
		"Pop"
	],
	//Question 88
	[
		"Địa lý",
		"Cầu Mỹ Thuận Bắc qua con sông nào ?",
		"Sông Hồng",
		"20",
		"Sông thương",
		"5",
		"Sông Tiền",
		"60",
		"Sông Cửu Long",
		"15",
		"Sông Tiền"
	],
	//Question 89
	[
		"Sinh học",
		"Đặc điểm nào sau đây đã có ở vượn người ?",
		"Đôi Tay Đã Tự Do Khi Đi",
		"20",
		"Đứng Thẳng",
		"5",
		"Biểu Lộ Tình Cảm Vui Buồn",
		"60",
		"Tư Duy Trìu Tượng phức Tạp",
		"15",
		"Biểu Lộ Tình Cảm Vui Buồn"
	],
	//Question 90
	[
		"Khoa học",
		"Bạn hãy Cho Biết Người đầu tiên đạt giải nobel  khu vực đông nam á mang quốc tịch nước nào ?",
		"Việt Nam",
		"70",
		"Lào",
		"5",
		"Thái Lan",
		"10",
		"Indonesia",
		"15",
		"Việt Nam"
	],
	//Question 91
	[
		"Địa lý",
		"Quốc Kỳ Cộng hoà Liên Bang Đức Có Bao Nhiêu Màu ?",
		"2",
		"10",
		"3",
		"60",
		"4",
		"20",
		"5",
		"10",
		"3"
	],
	//Question 92
	[
		"Kinh tế",
		"Tên viết tắt  của ngân hàng phát triển châu á ?",
		"ADP",
		"20",
		"UDP",
		"5",
		"ADB",
		"60",
		"ASK",
		"15",
		"ADB"
	],
	//Question 93
	[
		"Địa lý",
		"Tên Dãy núi lớn nhất thế giới ?",
		"Andes",
		"3",
		"Alps",
		"5",
		"Trường Sơn",
		"10",
		"Himalaya",
		"55",
		"Himalaya"
	],
	//Question 94
	[
		"Địa lý",
		"Việt Nam  là Thành viên thứ bao nhiêu  của WTO ?",
		"140",
		"3",
		"145",
		"5",
		"151",
		"10",
		"150",
		"55",
		"150"
	],
	//Question 95
	[
		"Nghệ thuật",
		"Bộ Phim ” Trở Về ” ( 1994) Do ai đạo diễn ?",
		"Nguyễn Vĩnh Sơn",
		"3",
		"Đới Xuân Việt",
		"5",
		"Hải Ninh",
		"10",
		"Đặng Nhật Minh",
		"55",
		"Đặng Nhật Minh"
	],
	//Question 96
	[
		"Âm nhạc",
		"trong thời kỳ  âm nhạc cổ điển , khi mà các bản giao hưởng  đều có 4 chương  thì tác phẩm nào lại có 5 chương ?",
		"Hành Khúc thổ Nhĩ kỳ",
		"20",
		"Bốn Mùa – Vivaldi",
		"5",
		"Bản giao Hưởng đồng Quê",
		"60",
		"Sông Danube Xanh – Strauss",
		"15",
		"Bản giao Hưởng đồng Quê"
	],
	//Question 97
	[
		"Âm nhạc",
		"Bài Hát ” thanh Niên Làm Theo lời Bác ” Hay ” Đoàn Ca ” Là Sáng tác Của Ai ?",
		"Trần Tiến",
		"20",
		"Trần hiếu",
		"5",
		"Hoàng Hoà",
		"60",
		"không Phải 3 Tác giả Trên",
		"15",
		"Hoàng Hoà"
	],
	//Question 98
	[
		"Lịch sử",
		"Tổng thống john Kenede Bị Ám Sát vào Năm Nào ?",
		"1960",
		"20",
		"1962",
		"5",
		"1963",
		"60",
		"1965",
		"15",
		"1963"
	],
	//Question 99
	[
		"Thể thao",
		"Quốc Gia Nào giành nhiều huy chương vàng thứ 3  Olympic  Athens 2004 ?",
		"Anh",
		"20",
		"Nga",
		"55",
		"Hy Lạp",
		"10",
		"Tây ban Nha",
		"15",
		"Nga"
	],
	//Question 100
	[
		"Âm nhạc",
		"Nhạc Cụ Gõ Cổ Nhất Của Nước ta Tên Là Gì ?",
		"Không Phải 3 loại Trên",
		"30",
		"Trống Đồng",
		"5",
		"Trống Đá",
		"10",
		"Đàn Đá",
		"55",
		"Đàn Đá"
	]
];