import device;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import ui.View as View;
import ui.widget.ButtonView as ButtonView;
import src.Config.MainMenuScreenConfig as config;
import src.lib.uiInflater as uiInflater;


var BG_WIDTH = config.bgWidth;
var BG_HEIGHT = config.bgHeight;

exports = Class(ImageView, function (supr) {
  this.init = function (opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      image: "resources/images/menuBackground.png"
    });

    supr(this, 'init', [opts]);
    console.log("hungnt main menu screen");
    this.build();
  };
  this.build = function () {

    uiInflater.addChildren(config.MainUI.TitleView.children, this);
    uiInflater.addChildren(config.MainUI.ProfileStat.children, this);
    uiInflater.addChildren(config.MainUI.ProfileImage.children, this);

    var playButton = new ButtonView(merge({parent: this, y: 400}, config.MainUI.GameModes))
    uiInflater.addChildren(config.MainUI.GameModes.children, playButton);

    playButton.duelButton.onInputSelect = bind(this, function(){
      console.log("hungnt mm input");
      this.emit('MainMenuScreen:play');
    });
  }
});
