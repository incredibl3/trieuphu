import device;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;
var ProfileStatY = 150;
var ProfileImageY = 250;
var ANSWER_WIDTH = BG_WIDTH - 80;
var ANSWER_HEIGHT = 80;
var QUESTIONBG_WIDTH = BG_WIDTH - 100;
var QUESTIONBG_HEIGHT = 300;
var ITEM_WIDTH = 110;
var ITEM_HEIGHT = 80;

exports = {
  bgWidth: BG_WIDTH,
  bgHeight: BG_HEIGHT,
  answerWidth: ANSWER_WIDTH,
  answerHeight: ANSWER_HEIGHT,
  questionBG_width: QUESTIONBG_WIDTH,
  questionBG_height: QUESTIONBG_HEIGHT,
  // TimerView: {
  //   layout: 'box',
  //   x: 0,
  //   y: 0,
  //   width: BG_WIDTH,
  //   height: 15,
  //   backgroundColor: "white",
  // },
  TimerView: {
    name: 'TimerView',
    x: 0,
    y: 0,
    width: BG_WIDTH,
    height: 15,
    zIndex: 5,
    children: [
      {
          cls: "ui.ImageView",
          name: "base",
          x: 0,
          y: 0,
          width: BG_WIDTH,
          height: 51,
          zIndex: 1,
          image: "resources/images/base.png",
      },
      {
          cls: "ui.ImageView",
          name: "progress",
          x: 5,
          y: 32,
          width: 80,
          height: 15,
          zIndex: 1,
          image: "resources/images/progress.png",
      },
      {
          cls: "ui.ImageView",
          name: "worm",
          x: 5,
          y: 0,
          width: 93,
          height: 51,
          zIndex: 1,
          image: "resources/images/worm.png",
      }
    ]
  },
  scoreView: {
    x: 8,
    y: 55,
    width: BG_WIDTH,
    height: 75,
    text: "0/20",
    horizontalAlign: "center",
    spacing: 0,
    zIndex: 1,
    characterData: {
      "0": { image: "resources/images/score_0.png" },
      "1": { image: "resources/images/score_1.png" },
      "2": { image: "resources/images/score_2.png" },
      "3": { image: "resources/images/score_3.png" },
      "4": { image: "resources/images/score_4.png" },
      "5": { image: "resources/images/score_5.png" },
      "6": { image: "resources/images/score_6.png" },
      "7": { image: "resources/images/score_7.png" },
      "8": { image: "resources/images/score_8.png" },
      "9": { image: "resources/images/score_9.png" },
      "/": { image: "resources/images/slash.png" }
    },
    // compositeOperation: "lighter"
  },
  GameLayer: {
    name: 'GameLayer',
    x: 0,
    y: 0,
    width: BG_WIDTH,
    height: BG_HEIGHT - 100,
    direction: "vertical",
    children: [
        {
          cls: "ui.ImageView",
          name: "questionCategoryBG",
          x: 80,
          y: 110,
          width: 429,
          height: 109,
          zIndex: 5,
          image: "resources/images/ribbon.png",
        },
        {
          name: "categoryTxt",
          cls: "ui.TextView",
          text: 'Who is Billionaire',
          fontFamily: "UVNBaiSau_R",
          color: 'grey',
          // shadowColor: "black",
          x: 145,
          y: 90,
          size: 35,
          wrap: true,
          width: 300,
          height: 100,
          zIndex: 6
        },
        {
          cls: "ui.ImageView",
          name: "questionBG",
          x: 50,
          y: 100,
          width: QUESTIONBG_WIDTH,
          height: QUESTIONBG_HEIGHT,
          image: "resources/images/questionBackground.png",
          // scaleMethod: "9slice",
          // debug: false,
          // sourceSlices: {
          //   horizontal: {left: 16, center: 32, right: 16},
          //   vertical: {top: 16, middle: 32, bottom: 16}
          // },
          // destSlices: {
          //   horizontal: {left: 16, right: 16 },
          //   vertical: {top: 16 , bottom: 16 }
          // }
        },
        {
          name: "questionTxt",
          cls: "ui.TextView",
          text: 'Who is Billionaire',
          fontFamily: "UVNBaiSau_R",
          color: 'grey',
          // shadowColor: "black",
          x: 60,
          y: 140,
          size: 35,
          wrap: true,
          width: 450,
          height: QUESTIONBG_HEIGHT,
          zIndex: 1
        },
        {
          name: "answer1",
          cls: "ui.widget.ButtonView",
          layout: "box",
          centerX : true,
          y: 500,
          width: ANSWER_WIDTH,
          height: ANSWER_HEIGHT,
          title: "Answer 1",
          text: {
            color: "white",
            fontFamily: "UVNBaiSau_R",
            size: 40,
            horizontalAlign: "center",
            autoFontSize: true,
            autoSize: false
          },
          images: {
            "up": "resources/images/button_up.png",
            "down": "resources/images/button_down.png"
          }
        },
        {
          name: "answer2",
          cls: "ui.widget.ButtonView",
          layout: "box",
          centerX : true,
          y: 600,
          width: ANSWER_WIDTH,
          height: ANSWER_HEIGHT,
          title: "Answer 2",
          text: {
            color: "white",
            fontFamily: "UVNBaiSau_R",
            size: 40,
            horizontalAlign: "center",
            autoFontSize: true,
            autoSize: false
          },
          images: {
            "up": "resources/images/button_up.png",
            "down": "resources/images/button_down.png"
          }
        },
        {
          name: "answer3",
          cls: "ui.widget.ButtonView",
          layout: "box",
          centerX : true,
          y: 700,
          width: ANSWER_WIDTH,
          height: ANSWER_HEIGHT,
          title: "Answer 3",
          text: {
            color: "white",
            fontFamily: "UVNBaiSau_R",
            size: 40,
            horizontalAlign: "center",
            autoFontSize: true,
            autoSize: false
          },
          images: {
            "up": "resources/images/button_up.png",
            "down": "resources/images/button_down.png"
          }
        },
        {
          name: "answer4",
          cls: "ui.widget.ButtonView",
          layout: "box",
          centerX : true,
          y: 800,
          width: ANSWER_WIDTH,
          height: ANSWER_HEIGHT,
          title: "Answer 4",
          text: {
            color: "white",
            fontFamily: "UVNBaiSau_R",
            size: 40,
            horizontalAlign: "center",
            autoFontSize: true,
            autoSize: false
          },
          images: {
            "up": "resources/images/button_up.png",
            "down": "resources/images/button_down.png"
          }
        }
      ]
  },
  SupportItems: {
    name: 'SupportItems',
    x: 0,
    y: 0,
    width: BG_WIDTH,
    height: 100,
    direction: "horizontal",
    children: [
        {
          name: "item1",
          cls: "ui.widget.ButtonView",
          layout: "box",
          x: 0,
          width: ITEM_WIDTH,
          height: ITEM_HEIGHT,
          title: "Timer",
          toggleSelected : true,
          text: {
            color: "white",
            fontFamily: "UVNBaiSau_R",
            size: 30,
            autoFontSize: true,
            autoSize: false
          },
          images: {
            // "up": "resources/images/button_up.png",
            // "down": "resources/images/button_down.png",
            "unselected": "resources/images/button_up.png",
            // "selected": "resources/images/button_wrong.png",
            // "disabled": "resources/images/button_wrong.png",
          }
        },
        {
          name: "item2",
          cls: "ui.widget.ButtonView",
          layout: "box",
          x: 150,
          width: ITEM_WIDTH,
          height: ITEM_HEIGHT,
          title: "Public",
          toggleSelected : true,
          text: {
            color: "white",
            fontFamily: "UVNBaiSau_R",
            size: 30,
            autoFontSize: true,
            autoSize: false
          },
          images: {
            // "up": "resources/images/button_up.png",
            // "down": "resources/images/button_down.png",
            "unselected": "resources/images/button_up.png",
            // "selected": "resources/images/button_wrong.png",
            // "disabled": "resources/images/button_wrong.png",
          }
        },
        {
          name: "item3",
          cls: "ui.widget.ButtonView",
          layout: "box",
          x: 300,
          width: ITEM_WIDTH,
          height: ITEM_HEIGHT,
          title: "2 taps",
          toggleSelected : true,
          text: {
            color: "white",
            fontFamily: "UVNBaiSau_R",
            size: 30,
            autoFontSize: true,
            autoSize: false
          },
          images: {
            // "up": "resources/images/button_up.png",
            // "down": "resources/images/button_down.png",
            "unselected": "resources/images/button_up.png",
            // "selected": "resources/images/button_wrong.png",
            // "disabled": "resources/images/button_wrong.png",
          }
        },
        {
          name: "item4",
          cls: "ui.widget.ButtonView",
          layout: "box",
          x: 450,
          width: ITEM_WIDTH,
          height: ITEM_HEIGHT,
          title: "50/50",
          toggleSelected : true,
          text: {
            color: "white",
            fontFamily: "UVNBaiSau_R",
            size: 30,
            autoFontSize: true,
            autoSize: false
          },
          images: {
            // "up": "resources/images/button_up.png",
            // "down": "resources/images/button_down.png",
            "unselected": "resources/images/button_up.png",
            // "selected": "resources/images/button_wrong.png",
            // "disabled": "resources/images/button_wrong.png",
          }
        }
      ]
  }
};