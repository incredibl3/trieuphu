import device;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;
var ProfileStatY = 150;
var ProfileImageY = 250;

exports = {
  bgWidth: BG_WIDTH,
  bgHeight: BG_HEIGHT,
  MainUI: {
    name: 'UILayout',
    x: 0,
    y: 0,
    width: BG_WIDTH,
    height: BG_HEIGHT,
    TitleView:{
      name: "TitleView",
      layout: "linear",
      direction: "horizontal",
      zIndex: 2,
      children: [
        {
          cls: "ui.View",
          backgroundColor: 'black',
          x: 0,
          y: 0,
          width: BG_HEIGHT,
          height: 100
        },
        {
          name: "energyImg",
          cls: "ui.TextView",
          text: 'Who is Billionaire',
          color: 'white',
          x: 0,
          y: 0,
          size: 45,
          width: BG_WIDTH,
          height: 100
        }
      ]
    },
    ProfileStat: {
      name: "ProfileStat",
      layout: "linear",
      direction: "horizontal",
      children: [
        {
          name: "energyImg",
          cls: "ui.ImageView",
          width: 50,
          height: 50,
          x: 50,
          y: ProfileStatY,
          image: "resources/images/energy.png"
        },
        {
          name: "energyTxt",
          cls: "ui.TextView",
          x: 100,
          y: ProfileStatY,
          width: 100,
          height: 50,
          text: "3",
          color: "white"
        },
        {
          name: "gemImg",
          cls: "ui.ImageView",
          width: 50,
          height: 50,
          x: 400,
          y: ProfileStatY,
          image: "resources/images/gem.png"
        },
        {
          name: "gemTxt",
          cls: "ui.TextView",
          x: 450,
          y: ProfileStatY,
          width: 100,
          height: 50,
          text: "10",
          color: "white"
        }
      ]
    },
    ProfileImage: {
      name: "ProfileImage",
      layout: "linear",
      direction: "horizontal",
      children: [
        {
          name: "profileImg",
          cls: "ui.ImageView",
          width: 150,
          height: 150,
          x: 100,
          y: ProfileImageY,
          image: "resources/images/player.png"
        },
        {
          name: "profileTxt",
          cls: "ui.TextView",
          x: 250,
          y: ProfileImageY,
          width: 200,
          height: 50,
          text: "Fun1203 - Defender",
          color: "white"
        }
      ]
    },
    GameModes: {
      name: "GameModes",
      layout: "linear",
      direction: "vertical",
      children: [
        {
          name: "duelButton",
          cls: "ui.widget.ButtonView",
          layout: "box",
          centerX : true,
          centerY: true,
          width: BG_WIDTH - 80,
          height: 80,
          title: "DUEL",
          text: {
            color: "white",
            size: 40,
            autoFontSize: false,
            autoSize: false
          },
          images: {
            "up": "resources/images/blue1.png",
            "down": "resources/images/blue2.png"
          }
        }
      ]
    }
  }
};